﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Sky"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Cull Off
			ZWrite Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float3 dir : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

		/*	inline float3 WorldSpaceViewDir( in float4 v )
			{
				return _WorldSpaceCameraPos.xyz - mul(_Object2World, v).xyz;
			}*/
			float2 SphereNormalToUv( float3 n )
			{
				n = n.xzy;
				float3 b = n+float3(0, 0, 1);
				float p = 2*sqrt(dot(b,b));
				float2 uv = n.xy/p + .5;
				float2 ndc = uv*2-1;
				float2 c = normalize(ndc);
				ndc /= max(abs(c.x), abs(c.y));
				uv = ndc * .5 + .5;
				return uv;
			}

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.dir = v.vertex;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float4 hdr = tex2D(_MainTex, SphereNormalToUv(normalize(i.dir)));
			//	hdr.rgb = hdr.rgb / (hdr.rgb + 1);
				return hdr;
			}
			ENDCG
		}
	}
}
