﻿Shader "QSwitch/Carpet" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_SpecTex ("Specular", 2D) = "white" {}
		_Offset("Offset", Vector) = (0,0,0,0)
		_SpecMask("SpecMask", Float) = 0.1
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong vertex:vert 
		//nodirlightmap
		//#pragma target 3.0
		//#pragma debug
		

		sampler2D _MainTex;
		sampler2D _SpecTex;
		float4 _Offset;
		float _SpecMask;

		struct Input {
			float2 uv_MainTex;
			float3 worldRefl;
			float3 worldNormal;
			float3 worldPos;
			float3 worldViewDir;
		};
		
		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.worldViewDir = WorldSpaceViewDir(v.vertex);
		}
		
		half4 DoLight (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten, half3 lightColor)
		{
		//	float3 N = s.Normal;
			float3 N = float3(0,1,0);
			half3 h = normalize (lightDir + viewDir);

			half diff = saturate(dot (N, lightDir));
			
			float value = dot(s.Albedo,float3(1,1,1))*0.3333333;
			
			value = pow(value,10);

			float nh = saturate( dot (N, h));
			float gloss = 25 +  s.Specular*100;//pow(2,s.Gloss*13);
			float3 spec = pow(nh, gloss)* _SpecMask * _SpecColor;//s.Specular;

			half4 c;
			c.rgb = lightColor * spec * diff * atten * 10;
			c.a = 1;
			
			return c;
		}
		
		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 c2 = tex2D (_MainTex, IN.uv_MainTex*3);
			half4 specTex = tex2D (_SpecTex, IN.uv_MainTex*2);
			
			c = sqrt(c) * sqrt(c2);
			o.Albedo = c.rgb;
			o.Gloss = 1;
			o.Specular = specTex.r*0.05;
			_SpecColor = specTex * c;
			
			float3 R = normalize(IN.worldRefl);
			
			IN.worldPos.xz += _Offset.xy;
			
			float distToIntersect = 3.03 / R.y;
			float3 intersect = IN.worldPos + R * distToIntersect;
			float period = 4.55*2;
			intersect.xz = intersect.xz / period;
			float2 fr = frac(intersect.xz);
			intersect.xz = round(intersect.xz);
			intersect.xz = (intersect.xz * period);
			
			fr = fr*2-1;
			fr = abs(fr);
			fr = 1-fr;
			fr = pow(fr,2);
			fr = 1-fr;
			//fr = saturate(fr*2);
			//fr = 1;
			
			float atten = 1;
			atten = fr.x * fr.y;
			
		//	float value = _SpecColor.r;
		//	o.Albedo = value;//saturate(value*value * 8 - 0.125);
			
			
		//	IN.worldViewDir = (_WorldSpaceCameraPos - IN.worldPos);
			
			float3 L = intersect - IN.worldPos;
			o.Emission = DoLight(o, normalize(L), normalize(IN.worldViewDir), atten, float3(192,192,160)/255).rgb;
		//	o.Emission.r *= fr.x;
		//	o.Emission.g *= fr.y;
		
		
		/*	o.Albedo = 0;
			o.Gloss = 0;
			o.Specular = 0;*/
			
			
		//	o.Emission = normalize(-IN.worldViewDir)*0.5+0.5;
		//	o.Emission = float3(UNITY_MATRIX_MV[0][3], UNITY_MATRIX_MV[1][3], UNITY_MATRIX_MV[2][3])*0.01;
		//	o.Emission = float3(UNITY_MATRIX_MV[3][0], UNITY_MATRIX_MV[3][1], UNITY_MATRIX_MV[3][3])*1000.01;
		}
		ENDCG
	}
	
	
	
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 2.0

		sampler2D _MainTex;
		sampler2D _SpecTex;
		float4 _Offset;
		float _SpecMask;

		struct Input {
			float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 specTex = tex2D (_SpecTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Gloss = 1;
			o.Specular = specTex.r;
			_SpecColor = specTex * c;
		}
		ENDCG
	}
	
	FallBack "Specular"
}
