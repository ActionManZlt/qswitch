﻿Shader "QSwitch/LobbyFloor" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_SpecTex ("Specular", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
	//	#pragma surface surf Q
		#pragma surface surf BlinnPhong vertex:vert
		//#pragma target 3.0
		//#pragma only_renderers d3d9 d3d11
		
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		sampler2D _SpecTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldRefl;  
			float3 worldNormal; INTERNAL_DATA 
			float3 worldPos;
			float3 worldViewDir;
		};
		
		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.worldViewDir = WorldSpaceViewDir(v.vertex);
		}
				
		float3 tonemap( float3 light ) 
		{
			float3 result;

			float crush = 0.0;
			float frange = 13;
			float exposure = 300;
			
			result = log2(1+light*exposure);
			result = smoothstep(crush, frange, result);

		//	result = pow( result, 1/2.2 );

			return result;
		}
		
		
		half4 DoLight (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten, half3 lightColor)
		{
			half3 h = normalize (lightDir + viewDir);

			half diff = saturate(dot (s.Normal, lightDir));

			float nh = saturate( dot (s.Normal, h));
			float gloss = pow(2,s.Gloss*13);
			float spec = pow(nh, gloss)* s.Specular;
			//spec *= (gloss+1) * 2;

			half4 c;
			c.rgb = lightColor * spec * diff * atten * 10;
			c.a = 1;
			
			//c.rgb = c.rgb / (c.rgb+10);
			//c.rgb = tonemap(c.rgb);
			return c;
		}
		
		half4 LightingQ (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			return DoLight(s, lightDir, viewDir, atten, _LightColor0.rgb);
		}
		
		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 specTex = tex2D (_SpecTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Specular = specTex.r * 0.1;
			o.Gloss = 0.2 + (1-specTex.g)*0.5;//*0.7;//+0.1;
		//	o.Gloss = (abs(c.r-0.25))*1 + 0.75;
		//	o.Gloss = 0.59;
		
		//o.Normal = float3(0,1,0);
		
		//	IN.worldNormal = WorldNormalVector(IN, o.Normal);
		//	IN.worldRefl = WorldReflectionVector(IN, o.Normal);
		
			//IN.worldNormal = float3(0,1,0);
			float3 R = IN.worldRefl;
			float3 N = IN.worldNormal;
			float3 V = normalize(IN.worldViewDir);
			R = reflect(-V, IN.worldNormal);
			//R = normalize(R);
			
		//	R = abs(IN.worldPos);
			
			
			float px = R.x < 0 ? 16.71 : 1.89;
			float3 planePos = float3(px,0,0);
			//float3 planeDir = float3(1,0,0);
			float3 planeDir = float3(R.x < 0 ? 1 : -1,0,0);
			
			float distToPlane = dot( IN.worldPos + planePos, planeDir );
			
			float d = (dot(planeDir, -R));
			
			float distToIntersect = distToPlane/d;
			
			
			float deskShadow=1, columnShadow = 1, frontShadow = 1;
			//if( distToIntersect > 0 )
			//distToIntersect = abs(distToIntersect);
			{
				float3 intersect = IN.worldPos + R * distToIntersect;
			
				
				//o.Emission.r = abs(distToIntersect);
				//o.Emission.g = d < 0 ? 1 : 0;
				//o.Emission.b = distToPlane < 0 ? 1 : 0;
				//o.Emission = abs(planePos + IN.worldPos).rrr/10;
				
				float post1 = -27.96;
				float post2 = -22.58;
				float postSpacing = post2 - post1;
				float postRad = 0.675;
				//postRad = min(postRad/d, postSpacing*0.2);
				postRad = min(postRad/d, postRad*1.7);
				
				float postFrac = frac((intersect.z-post1) / postSpacing + 0.5)-0.5;
				//postFrac = (1-abs(postFrac-0.5)) * postSpacing;
				postFrac = postFrac * postSpacing;
				//postFrac = intersect.z-post1;
				
				float3 spherePos = intersect;
				//spherePos.x -= 0.68*0.5;
				//spherePos.y += 10;
				//spherePos.z += 10;
				spherePos.z += postFrac;
				
				float3 cr = cross(R, spherePos - IN.worldPos);
				float distFromPointToRay = length(cr);
				//distFromPointToRay = cr.z;
				
				columnShadow = abs(distFromPointToRay);
				
				//postRad = 0;
				
			//	columnShadow = abs(intersect.z - (-27.96));
				//float shadowWidth = 0.5;
				float shadowWidth = 1;//(1-o.Gloss);
				
				float distScale = abs(distToIntersect)*0.2;
				distScale = abs(distScale-2);
				//distScale = max(distScale,1);
				shadowWidth *= distScale*distScale;
				shadowWidth += 0.1;
				postRad -= 0.2;
				//postRad = postRad/max(0.4,distScale*distScale) * 0.1;
				columnShadow = saturate((columnShadow - postRad) / shadowWidth);
			}
			
			{
			//-4.76, -13.85
			//-12.47
				float distToPlane = IN.worldPos.z + 12.47;
				float distToIntersect = distToPlane/-R.z;
				distToIntersect = abs(distToIntersect);
				distToIntersect = min(distToIntersect, abs(distToPlane)/(abs(R.x)));
				float3 spherePos = IN.worldPos + R * distToIntersect;
				spherePos.x = min(max(spherePos.x, -13.85+1), -4.76-1);
				spherePos.y = min(max(spherePos.y, 9.24), 18.67);
				spherePos.z = -12.47;
				float3 cr = cross(R, spherePos - IN.worldPos);
				float distFromPointToRay = length(cr);
				
			//	if( dot(R, spherePos - IN.worldPos) > 0 )
			//	distFromPointToRay = 9000;
				
			//	distFromPointToRay = length( spherePos - IN.worldPos );
				
			//	o.Emission = saturate(distFromPointToRay-2);
			//	o.Emission = (distToIntersect);
			
				frontShadow = saturate(distFromPointToRay-1);
			}
			
			float specFade = 1-saturate( (abs(IN.worldPos.x + 9.305)-(6.735-0.5)) );//2.57 , 16.04
			
			float lx = R.x < 0 ? -20.08 : 1.47;
			float3 L0 = float3(lx, 16.82, -34.03+3) - IN.worldPos;
			float3 L1 = float3(lx, 16.82, -8.43) - IN.worldPos;
			float3 Ld = L1 - L0;
			
			float t = (dot(R,L0)*dot(R,Ld) - dot(L0,Ld))
					/ (dot(Ld,Ld) - dot(R,Ld)*dot(R,Ld));
					
					
		//	t = (dot(N,L0)*dot(N,Ld) - dot(L0,Ld))
		//		/ (dot(Ld,Ld) - dot(N,Ld)*dot(N,Ld));
		
			t = saturate(t);
			float3 L = L0 + t*Ld;
			
			
			
		/*	if(0)
			{
				//7.49, 11.11, 10.08, 19.97
				float3 d0 = float3(-11.11, 10.08-.82, -19.97) - IN.worldPos;
				float3 d1 = float3(-7.49, 10.08-.82, -19.97) - IN.worldPos;
				float3 dd = d1 - d0;
				float t = (dot(R,d0)*dot(R,dd) - dot(d0,dd))
						/ (dot(dd,dd) - dot(R,dd)*dot(R,dd));
				t = saturate(t);
				float3 spherePos = IN.worldPos + d0 + t*dd;
				
				float3 cr = cross(R, spherePos - IN.worldPos);
				cr.zx*= 5;
				float distFromPointToRay = length(cr);
				//cr = abs(cr);
				//distFromPointToRay = cr.x + cr.y*0.25 + cr.z;
				
				//distFromPointToRay = length(d1);
				
				deskShadow = saturate((distFromPointToRay-1.77*1.4)*3);
				
			//	o.Albedo = 0;
			//	o.Albedo.g = (distFromPointToRay);
			}*/
			
			if(1)
			{
				float2 center = float2(-9.305, -20.095);
				float2 size1 = float2(5.13, 1.77)/2;
				float2 size2 = size1;
				
				float2 scale1 = 1;
				float2 scale2 = scale1;
				
				float dist1, dist2, dist;
				
				float3 intersect = IN.worldPos;
					intersect.xz -= center;
					intersect.xz = max( -size1, min( size2, intersect.xz ) );
					intersect.xz += center;
					dist = length(intersect.xz - IN.worldPos.xz);
				//	dist1 = max( intersect.x-size1.x, (intersect.x-size2.x)*scale2.x );
				//	dist2 = max( intersect.z-size1.x, (intersect.z-size2.y)*scale2.y );
				//	dist = max(dist1,dist2);
			//	o.Emission.r = intersect.x - IN.worldPos.x;
			//	o.Emission.g = IN.worldPos.x - intersect.x;
				float2 dirToDesk = intersect.xz - IN.worldPos.xz;
				//float mask = saturate( dot(dirToDesk, R.xz) * 10 + 1 );
			//	dirToDesk = pow(dirToDesk,1);
				float mask = dot(dirToDesk, R.xz);
				
				mask = saturate( mask * 10 + 0.2 ) * saturate((0.5/R.y)-length(dirToDesk));
				
				
				float2 corner0 = center + size1 * float2(-1,-1);
				float2 corner1 = center + size1 * float2( 1,-1);
				float2 corner2 = center + size2 * float2(-1, 1);
				float2 corner3 = center + size2 * float2( 1, 1);
				
				float2 cd0 = corner0 - IN.worldPos.xz;
				float2 cd1 = corner1 - IN.worldPos.xz;
				float2 cd2 = corner2 - IN.worldPos.xz;
				float2 cd3 = corner3 - IN.worldPos.xz;
				
				
				
				cd0 = cd0.yx * float2(1,-1);
				cd1 = cd1.yx * float2(1,-1);
				cd2 = cd2.yx * float2(1,-1);
				cd3 = cd3.yx * float2(1,-1);
				
				
				
				float dot0 = dot(cd0, R.xz);
				float dot1 = dot(cd1, R.xz);
				float dot2 = dot(cd2, R.xz);
				float dot3 = dot(cd3, R.xz);
				
				
				float left =  (saturate(dot0)+saturate(dot1)+saturate(dot2)+saturate(dot3));	
				float right = (saturate(-dot0)+saturate(-dot1)+saturate(-dot2)+saturate(-dot3));	
				
				deskShadow *= 1-saturate(left*right*mask*30);
				//deskShadow *= 1-saturate(mask*3);
				//o.Emission.b = (deskShadow);	
				/*
					
				float distToIntersect = (10.08 - IN.worldPos.y) / R.y;
				intersect = IN.worldPos + R * distToIntersect;
				
				intersect.xz -= float2(-9.305, -20.095);
				
				
			//	scale1.x /= saturate(R.x)+0.001;
			//	scale1.y /= saturate(R.z)+0.001;
			
			size1.x *= 1+(saturate(-R.x)*abs(R.y));
			
				
				 dist1 = max( (-size1.x-intersect.x)*scale1.x, (intersect.x-size2.x)*scale2.x );
				 dist2 = max( (-size1.y-intersect.z)*scale1.y, (intersect.z-size2.y)*scale2.y );
				// dist = max(dist1,dist2);
				
				
				
				//dist = ( -11.87-intersect.x );
				deskShadow = saturate(dist*10);*/
			/*	o.Albedo = 0;
				o.Albedo.g = abs(distToIntersect)*0.01;
				o.Emission = 1-saturate(dist*10);
				shadow = 0;*/
			}
			
			
			{
				float distToIntersect = (18.67 - IN.worldPos.y) / R.y;
				float3 intersect = IN.worldPos + R * distToIntersect;
				
				//x 7.62 10.99, 9.305
				//z 17.52 27.94, 22.73
				float2 center = float2(-9.305, -22.73);
				float2 hw = float2(3.37, 10.42)/2;
					intersect.xz -= center;
					intersect.xz = max( -hw, min( hw, intersect.xz ) );
					intersect.xz += center;
					
				float3 SkyL = intersect - IN.worldPos;
					
			//	o.Emission = normalize(-SkyL)*0.5+0.5;
				float atten = deskShadow*specFade;
				float inFrontArea = 1-saturate(IN.worldPos.z+13.5);
				atten *= saturate(frontShadow+inFrontArea);
				o.Emission = DoLight(o, normalize(SkyL), normalize(IN.worldViewDir), atten, float3(149,161,236)/255).rgb;
			}
		
		
			float atten = length(L);
			atten = 1/(atten);
			atten = 1;
			
			//atten *= h;
			
			atten *= columnShadow*frontShadow*deskShadow*specFade;
			
			//o.Emission = 1-shadow;
			o.Emission += DoLight(o, normalize(L), normalize(IN.worldViewDir), atten*1, float3(.8,.9,1)).rgb;
			
			//o.Emission = normalize(IN.worldViewDir);
			
		/*	o.Albedo = 0;
			o.Specular = 0;
			o.Gloss = 0;*/
			o.Gloss = 1;
			o.Specular = 1;
			_SpecColor = specTex.r;
		//	o.Emission = intersect.z + 28.0;
		//	o.Emission.b = R.x < 0 ? 0 : 1;
		
		
		//	o.Emission = R;
		}
		ENDCG
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong

		sampler2D _MainTex;
		sampler2D _SpecTex;

		struct Input {
			float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 specTex = tex2D (_SpecTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Gloss = 1;
			o.Specular = 1;
			_SpecColor = specTex.r;
		}
		ENDCG
	}
	
	FallBack "Specular"
}
