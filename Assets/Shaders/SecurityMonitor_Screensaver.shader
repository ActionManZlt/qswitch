﻿Shader "QSwitch/SecurityMonitorScreensaver" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TitleTex ("Base (RGB)", 2D) = "white" {}
		_BarrelPower ("BarrelPower", Float ) = 1
		_TextOffset("TextOffset", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _TitleTex;
		float _BarrelPower;
		float _TextOffset;

		struct Input {
			float2 uv_MainTex;
		};
		

		float2 Distort(float2 p)
		{
			p = p*2-1;
		    
		    float l = length(p);
		    p /= l;
		    l = pow(l,_BarrelPower);
		    p*= l;
		    return p*0.5+0.5;
		}
		
		void surf (Input IN, inout SurfaceOutput o) {
		
			float2 uv = IN.uv_MainTex;
			float f = frac(uv.y*128 + _Time.y*0.5);
			float f2 = frac(uv.x*256);
			
			float2 offset = uv * float2(320,256)/2 - float2(15,256-15)/2;
			offset = round(offset);
			float dist = max( abs(offset.x), abs(offset.y) );
			dist = 1-saturate(dist-4);
			
			
			f = abs(f*2-1);
			f2 = abs(f2*2-1);
			
			f = f*0.5+.75;
			f2 = f2*0.0625+0.9375;
			
			IN.uv_MainTex += float2(-1.0/256, 1.0/128)*f;
			
			IN.uv_MainTex = Distort(IN.uv_MainTex);
			
			float2 px = float2(1.0/320, 1.0/256); 
			
			
			float2 nuv = IN.uv_MainTex-0.5;
			
			float t = floor(_Time.y*10)*0.1 * 0.05;
			float t0 = 1-frac(t);
			float t1 = 1-frac(t+0.25);
			float t2 = 1-frac(t+0.5);
			float t3 = 1-frac(t+0.75);
			
			float2 uv0 = nuv * t0 * float2(1,0.25) + float2(0.5, (0+0.5)*0.25);
			float2 uv1 = nuv * t1 * float2(1,0.25) + float2(0.5, (1+0.5)*0.25);
			float2 uv2 = nuv * t2 * float2(1,0.25) + float2(0.5, (2+0.5)*0.25);
			float2 uv3 = nuv * t3 * float2(1,0.25) + float2(0.5, (3+0.5)*0.25);
			
			half4 c0 = tex2D (_MainTex, uv0*10) * (1-t0);
			half4 c1 = tex2D (_MainTex, uv1*8) * (1-t1);
			half4 c2 = tex2D (_MainTex, uv2*6) * (1-t2);
			half4 c3 = tex2D (_MainTex, uv3*4) * (1-t3);
			
			half4 c = c0 + c1 + c2 + c3;
					
			float2 textUv = uv*10-9;
			float2 textUvTest = saturate(textUv - float2(0,0.5));
			float textAlpha = dot( textUvTest, textUvTest.yx ) > 0 ? 1 : 0;
			textUv.y -= _TextOffset;
			half4 text = tex2D(_TitleTex, textUv);
			
			c.rgb += text.rgb * textAlpha;
			        
			c.rgb = lerp(c.rrr, half3(1,0,0), dist*0.75);
			
			half3 color =  c.rgb * f * f2  * 0.25;
			o.Emission = color;
		}
		ENDCG
	} 
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 2.0

		sampler2D _MainTex;
		sampler2D _TitleTex;
		float _BarrelPower;
		float _TextOffset;

		struct Input {
			float2 uv_MainTex;
		};
		
		
		void surf (Input IN, inout SurfaceOutput o) {
		
			float2 uv = IN.uv_MainTex;
			float f = frac(uv.y*128 + _Time.y*0.5);
			float f2 = frac(uv.x*256);
			
			float2 offset = uv * float2(320,256)/2 - float2(15,256-15)/2;
			offset = round(offset);
			float dist = max( abs(offset.x), abs(offset.y) );
			dist = 1-saturate(dist-4);
			
			
			f = abs(f*2-1);
			f2 = abs(f2*2-1);
			
			f = f*0.5+.75;
			f2 = f2*0.0625+0.9375;
			
			IN.uv_MainTex += float2(-1.0/256, 1.0/128)*f;
			
			
			float2 px = float2(1.0/320, 1.0/256); 
			
			
			float2 nuv = IN.uv_MainTex-0.5;
			
			float t = floor(_Time.y*10)*0.1 * 0.05;
			float t0 = 1-frac(t);
			float t1 = 1-frac(t+0.5);
			
			float2 uv0 = nuv * t0 * float2(1,0.25) + float2(0.5, (0+0.5)*0.25);
			float2 uv1 = nuv * t1 * float2(1,0.25) + float2(0.5, (1+0.5)*0.25);
			
			half4 c0 = tex2D (_MainTex, uv0*10) * (1-t0);
			half4 c1 = tex2D (_MainTex, uv1*8) * (1-t1);
			
			half4 c = c0 + c1;
			
			half3 color =  c.rgb * f * f2  * 0.25;
			o.Emission = color;
		}
		ENDCG
	} 
	FallBack "Self-Illum/Diffuse"
}
