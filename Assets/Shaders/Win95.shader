﻿Shader "Custom/Win95" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
		
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			float y = saturate(IN.uv_MainTex.y);
			float f = 1;
			float cutoff = (13.5/512);
			#if UNITY_UV_STARTS_AT_TOP 
			 f = 1-saturate((y - cutoff)*256);
			#else
			 f = 1-saturate((1-y - cutoff)*256);
			#endif
			
			IN.uv_MainTex.x += frac(_Time.x*5);
			half4 c2 = tex2D (_MainTex, IN.uv_MainTex);
			o.Emission = lerp( c.rgb, c2.rgb, f );
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
