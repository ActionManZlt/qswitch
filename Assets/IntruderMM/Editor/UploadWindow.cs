﻿using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Steamworks;
using UnityEditor;
using UnityEngine;

namespace Assets.IntruderMM.Editor
{
    class UploadWindow : EditorWindow
    {
        string mapname = "";
        string description = "";
        string changeNotes = "";
        string titleImagePath;
        string previewImagePath;
        string itemId;
        int readyToUpload = 0;
        bool awoken;
        bool uploading;
        Vector2 scrollPosition = new Vector2();
        bool[] selectedTags = new bool[UploadFileParameters.PossibleTags.Length];

        private float uploadProgressInterval = 1f;
        private float timeSinceLastUpdate;

        private float steamRefreshInterval = 1f;
        private float timeSinceLastRefresh;
        GUIStyle myTextAreaStyle;
        public static UploadWindow Instance;
        private bool savePrefs = true;

        private readonly string[] _options = {
            SteamworksWrapper.WorkshopItem.Visibility.Public.ToString(),
            SteamworksWrapper.WorkshopItem.Visibility.FriendsOnly.ToString(),
            SteamworksWrapper.WorkshopItem.Visibility.Private.ToString(),
            SteamworksWrapper.WorkshopItem.Visibility.NoChange.ToString()
        };

        private int _index;

        private GUIStyle imageStyle;
        private Texture2D titleImage;
        Texture2D previewImage;

        void OnEnable()
        {
            Instance = this;

            if (!awoken)
                Start();
        }

        void Start()
        {
            readyToUpload = 0;

            LoadImages();

            if (PlayerPrefs.HasKey("ExportLevel.mapname"))
            {
                mapname = PlayerPrefs.GetString("ExportLevel.mapname");
            }

            if (PlayerPrefs.HasKey("ExportLevel.visibility"))
            {
                var storedVisibiltiy = PlayerPrefs.GetString("ExportLevel.visibility");

                if (_options != null)
                {
                    for (var i = 0; i < _options.Length; i++)
                    {
                        if (_options[i] != storedVisibiltiy) continue;
                        _index = i;
                        break;
                    }
                }
            }

            if (PlayerPrefs.HasKey("ExportLevel.description"))
            {
                description = PlayerPrefs.GetString("ExportLevel.description");
            }

            if (PlayerPrefs.HasKey("ExportLevel.changeNotes"))
            {
                changeNotes = PlayerPrefs.GetString("ExportLevel.changeNotes");
            }

            if (PlayerPrefs.HasKey("ExportLevel.itemId"))
            {
                itemId = PlayerPrefs.GetString("ExportLevel.itemId");
            }

            awoken = true;
        }

        private void LoadImages()
        {
            titleImagePath = Application.dataPath + "/IntruderMM/Images/title.png";
            titleImage = new Texture2D(500, 96);
            if (File.Exists(titleImagePath))
            {
                
                titleImage.LoadImage(File.ReadAllBytes(titleImagePath));
            }

            previewImagePath = Application.dataPath + "/IntruderMM/Images/preview.jpg";

            string imagePath = previewImagePath;

            if (PlayerPrefs.HasKey("ExportLevel.previewImage"))
            {
                string tryImagePath = PlayerPrefs.GetString("ExportLevel.previewImage");

                if (File.Exists(tryImagePath))
                    imagePath = tryImagePath;
            }

            if (!string.IsNullOrEmpty(imagePath) && File.Exists(imagePath))
                previewImagePath = imagePath;

            previewImage = new Texture2D(500, 250);
            if (!File.Exists(previewImagePath)) return;
            
            previewImage.LoadImage(File.ReadAllBytes(previewImagePath));
        }

        void HandleSaveClosePressed()
        {
            SavePrefs();
            Close();
        }

        void SavePrefs()
        {
            if (!savePrefs)
                return;

            var rgx2 = new Regex("[^a-zA-Z0-9 -]");

            if (!string.IsNullOrEmpty(mapname))
                mapname = rgx2.Replace(mapname, "");

            PlayerPrefs.SetString("ExportLevel.mapname", mapname);
            PlayerPrefs.SetString("ExportLevel.description", description);
            PlayerPrefs.SetString("ExportLevel.changeNotes", changeNotes);
            PlayerPrefs.SetString("ExportLevel.previewImage", previewImagePath);
            PlayerPrefs.SetString("ExportLevel.itemId", itemId);
            PlayerPrefs.SetString("ExportLevel.visibility", _options[_index]);
            PlayerPrefs.Save();
        }

        void OnDisable()
        {
            SavePrefs();
        }

        void OnGUI()
        {
            if (!awoken)
            {
                Start();
            }

            if (previewImage == null || titleImage == null)
            {
                LoadImages();
            }

            timeSinceLastRefresh += Time.deltaTime;
            if (ExportLevel.wrapper != null && timeSinceLastRefresh >= steamRefreshInterval)
            {
                timeSinceLastRefresh = 0f;
                ExportLevel.wrapper.Update();
            }

            if (myTextAreaStyle == null || myTextAreaStyle.wordWrap == false)
            {
                myTextAreaStyle = new GUIStyle(EditorStyles.textArea);
                myTextAreaStyle.wordWrap = true;
            }

            if (Event.current != null)
                RenderGui();
        }

        private void RenderGui()
        {
            if (EditorApplication.isCompiling || readyToUpload == 1 || uploading)
            {
                GUILayout.BeginVertical();
                GUILayout.Box(titleImage, GUILayout.Height(96), GUILayout.Width(500));
                GUILayout.Label("Waiting for compilation...");
                GUILayout.EndVertical();

                ProcessUpload();

                return;
            }
            
            EditorUtility.ClearProgressBar();
            
            scrollPosition = GUILayout.BeginScrollView(scrollPosition);
            
            BeginPadding();
            GUILayout.Box(titleImage, GUILayout.Height(96), GUILayout.Width(500));
            EndPadding();

            BeginPadding();
            GUILayout.Box(previewImage, GUILayout.Height(250), GUILayout.Width(500));
            EndPadding();

            BeginPadding();
            if (GUILayout.Button("Select Preview Image"))
            {
                HandleFilePressed();
            }

            GUILayout.Label("(512px x 512px preferred)");

            EndPadding();

            GUILayout.Space(24);

            BeginPadding();
            EditorGUILayout.LabelField("Map Name:", GUILayout.Width(96));
            mapname = EditorGUILayout.TextField("", mapname, GUILayout.ExpandWidth(false), GUILayout.Width(400));
            EndPadding();

            GUILayout.Space(10);
            BeginPadding();
            EditorGUILayout.LabelField("Leave Workshop ID blank for new map", GUILayout.Width(502));
            EndPadding();

            BeginPadding();
            EditorGUILayout.LabelField("Workshop ID:", GUILayout.Width(96));
            itemId = EditorGUILayout.TextField("", itemId, GUILayout.ExpandWidth(false), GUILayout.Width(124));

            EditorGUILayout.LabelField("Visibility:", GUILayout.Width(64));
            _index = EditorGUILayout.Popup(_index, _options);
            EndPadding();

            // BeginPadding();
            // EditorGUILayout.LabelField("Description:", GUILayout.Width(96));
            // description = EditorGUILayout.TextArea(description, GUILayout.Height(64), GUILayout.Width(400));
            // EndPadding();

            BeginPadding();
            EditorGUILayout.LabelField("Change Notes:", GUILayout.Width(96));
            changeNotes = EditorGUILayout.TextArea(changeNotes, myTextAreaStyle, GUILayout.Height(64), GUILayout.Width(400));
            EndPadding();

            BeginPadding();

            string workshopButtonText = "Edit Workshop Maps";
            string uploadButtonText = "Upload New Map";

            if (!string.IsNullOrEmpty(itemId))
            {
                workshopButtonText = "Edit Map Details in Workshop";
                uploadButtonText = "Upload Map Update";
            }

            if (GUILayout.Button(uploadButtonText))
            {
                HandleUploadPressed();
            }

            if (GUILayout.Button(workshopButtonText))
            {
                HandleViewMapInWorkshop();
            }

            if (GUILayout.Button("Save & Close"))
            {
                HandleSaveClosePressed();
            }

            EndPadding();

            BeginPadding();

            if (!string.IsNullOrEmpty(itemId))
            {
                GUILayout.BeginVertical();
                EditorGUILayout.LabelField("Tags:", GUILayout.Width(496));
                GUILayout.Space(10);
                for (int i = 0; i < UploadFileParameters.PossibleTags.Length; i++)
                {
                    selectedTags[i] = GUILayout.Toggle(selectedTags[i], UploadFileParameters.PossibleTags[i]);
                }

                GUILayout.Space(5);
                if (GUILayout.Button("Set Tags", GUILayout.Width(100)))
                {
                    HandleSetTagsPressed(selectedTags);
                }

                GUILayout.EndVertical();
            }

            EndPadding();

            GUILayout.EndScrollView();

            // if (GUILayout.Button("Clear Prefs", GUILayout.Width(100)))
            // {
            //     savePrefs = false;
            //     PlayerPrefs.DeleteAll();
            //     Close();
            // }

            if (EditorApplication.isCompiling || readyToUpload == 1 || uploading)
            {
                ProcessUpload();
            }
            else
            {
                EditorUtility.ClearProgressBar();
            }
        }

        private static void EndPadding()
        {
            if (EditorApplication.isCompiling)
                return;

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private static void BeginPadding()
        {
            if (EditorApplication.isCompiling)
                return;

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
        }
        
        private long GetFileSize(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath)) return 0;
            //if you don't have permission to the folder, Directory.Exists will return False
            if (Directory.Exists(Path.GetDirectoryName(imagePath)))
                return File.Exists(imagePath) ? new FileInfo(imagePath).Length : 0;
            //if you land here, it means you don't have permission to the folder
            return -1;
        }

        private void HandleFilePressed()
        {
            var imagePath = EditorUtility.OpenFilePanel("Select preview image", "", "");
            if (previewImagePath.Length == 0) return;

            var fileSize = GetFileSize(imagePath);

            if (fileSize == -1)
            {
                Debug.Log("Permission to " + imagePath + " denied.");
                return;
            }
            if (fileSize == 0)
            {
                Debug.Log(imagePath + "The chosen file either does not exist or the filesize is 0.");
                return;
            }
            if(fileSize > 1000000)
            {
                Debug.Log(imagePath + " is too large, please choose a smaller image.");
                return;
            }
            
            var fileContent = File.ReadAllBytes(imagePath);
            previewImage.LoadImage(fileContent);
            previewImagePath = imagePath;
        }

        private void ProcessUpload()
        {
            ExportLevel.EnsureWrapperInitialization();
            if (EditorApplication.isCompiling)
            {
                EditorUtility.DisplayProgressBar("Waiting for compilation", "waiting..." + readyToUpload, .25f);
                return;
            }

            if (readyToUpload == 1 && mapname != "")
            {
                readyToUpload = 0;
                uploading = true;
                ExportLevel.Upload();
            }

            if (uploading)
            {
                timeSinceLastUpdate += Time.deltaTime;
                if (timeSinceLastUpdate >= uploadProgressInterval)
                {
                    timeSinceLastUpdate = 0f;
                    if (ExportLevel.wrapper.IsUploadingItem())
                    {
                        if (string.IsNullOrEmpty(itemId) && PlayerPrefs.HasKey("ExportLevel.itemId"))
                        {
                            itemId = PlayerPrefs.GetString("ExportLevel.itemId");
                        }
                        EditorUtility.DisplayProgressBar("Uploading map files",
                            "Uploading files to Steam...", ExportLevel.wrapper.GetUploadProgress());
                    }
                }
            }

            if (ExportLevel.uploadcount >= 1)
            {
                uploading = false;
            }
        }

        private void HandleTestMapPressed()
        {
            if (string.IsNullOrEmpty(mapname))
            {
                Debug.Log("Map name not found, open the Upload Menu and give it a name!");
            }
            else
            {
                PlayerPrefs.SetString("ExportLevel.mapname", mapname);
                SceneTesting.CheckNameAndPlayInIntruder();
            }
        }

        private void HandleUploadPressed()
        {
            var rgx = new Regex("[^a-zA-Z0-9 -]");

            if (!string.IsNullOrEmpty(mapname))
                mapname = rgx.Replace(mapname, "");

            PlayerPrefs.SetString("ExportLevel.mapname", mapname);
            PlayerPrefs.SetString("ExportLevel.description", description);
            PlayerPrefs.SetString("ExportLevel.changeNotes", changeNotes);
            PlayerPrefs.SetString("ExportLevel.previewImage", previewImagePath);
            PlayerPrefs.SetString("ExportLevel.itemId", itemId);
            PlayerPrefs.SetString("ExportLevel.visibility", _options[_index]);

            PlayerPrefs.Save();
            if (!ExportLevel.ExportSceneForIntruderAs())
            {
                return;
            }
            readyToUpload = 1;
        }

        private void HandleSetTagsPressed(bool[] tags)
        {
            ExportLevel.UpdateTags(tags);
        }

        private void HandleViewMapInWorkshop()
        {
            ExportLevel.EnsureWrapperInitialization();

            if (string.IsNullOrEmpty(itemId))
            {
                Debug.LogWarning("Item ID is empty, opening your workshop page");
                Application.OpenURL("https://steamcommunity.com/my/myworkshopfiles/?appid=518150");
            }
            else
                Application.OpenURL("https://steamcommunity.com/sharedfiles/filedetails/?id=" + itemId);
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

    }
}