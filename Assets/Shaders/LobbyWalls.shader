﻿Shader "QSwitch/Lobby Walls" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque"  "DisableBatching"="True" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard vertex:vert 

		// Use shader model 3.0 target, to get nicer looking lighting
		//#pragma target 3.0
		
		#include "UnityCG.cginc"

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
			float3 worldViewDir;
			float3 worldRefl;
			float3 worldNormal;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		
		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.worldViewDir = WorldSpaceViewDir(v.vertex);
		}
		
		half4 DoLight (SurfaceOutputStandard s, half3 lightDir, half3 viewDir, half atten, half3 lightColor, half3 N)
		{
			half3 h = normalize (lightDir + viewDir);

			half diff = saturate(dot (N, lightDir)*0.5+0.5);

			float nh = saturate( dot (N, h));
			float gloss = pow(2, (1-s.Albedo.r)*10);//pow(2,s.Gloss*13);
			float spec = pow(nh, gloss)* s.Albedo.r * 0.1;//s.Specular;
			//spec *= (gloss+1) * 2;

			half4 c;
			c.rgb = lightColor * spec * diff * atten * 10;
			c.a = 1;
			
			//c.rgb = c.rgb / (c.rgb+10);
			//c.rgb = tonemap(c.rgb);
			return c;
		}
		
		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			
			//half4 c = tex2D (_MainTex, IN.uv_MainTex);
			//half4 specTex = tex2D (_SpecTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
			//o.Specular = specTex.r * 0.05 + 0.01;
			//o.Gloss = (specTex.g);
			//o.Gloss = .4 + (1-specTex.r)*.2;
			
			
			float3 R1 = IN.worldRefl;
			float3 R = normalize(R1);
		//	float Nlen = length(IN.worldNormal);
		//	float3 N = IN.worldNormal/Nlen;
			float3 N = normalize(IN.worldNormal);
			float3 V = normalize(IN.worldViewDir);
			//R = reflect(-V, N);
			
		//	Nlen = Nlen*2-1;
		//	o.Gloss *= Nlen*Nlen*Nlen;
			
				float inFrontArea = 1-saturate(IN.worldPos.z+9.5);
			
			float px = R.x < 0 ? 16.71 : 1.89;
			float3 planePos = float3(px,0,0);
			//float3 planeDir = float3(1,0,0);
			float3 planeDir = float3(R.x < 0 ? 1 : -1,0,0);
			
			float distToPlane = dot( IN.worldPos + planePos, planeDir );
			
			float d = (dot(planeDir, -R));
			
			float distToIntersect = distToPlane/d;
			
			#if 0
			float deskShadow=1, columnShadow = 1, frontShadow = 1;
			//if( distToIntersect > 0 )
			//distToIntersect = abs(distToIntersect);
			{
				float3 intersect = IN.worldPos + R * distToIntersect;
			
				
				//o.Emission.r = abs(distToIntersect);
				//o.Emission.g = d < 0 ? 1 : 0;
				//o.Emission.b = distToPlane < 0 ? 1 : 0;
				//o.Emission = abs(planePos + IN.worldPos).rrr/10;
				
				float post1 = -27.96;
				float post2 = -22.58;
				float postSpacing = post2 - post1;
				float postRad = 0.675;
				//postRad = min(postRad/d, postSpacing*0.2);
				postRad = min(postRad/d, postRad*1.7);
				
				float postFrac = frac((intersect.z-post1) / postSpacing + 0.5)-0.5;
				//postFrac = (1-abs(postFrac-0.5)) * postSpacing;
				postFrac = postFrac * postSpacing;
				//postFrac = intersect.z-post1;
				
				float3 spherePos = intersect;
				//spherePos.x -= 0.68*0.5;
				//spherePos.y += 10;
				//spherePos.z += 10;
				spherePos.z += postFrac;
				
				float3 cr = cross(R, spherePos - IN.worldPos);
				float distFromPointToRay = length(cr);
				//distFromPointToRay = cr.z;
				
				columnShadow = abs(distFromPointToRay);
				
				//postRad = 0;
				
			//	columnShadow = abs(intersect.z - (-27.96));
				//float shadowWidth = 0.5;
				float shadowWidth = 1;//(1-o.Gloss);
				
				float distScale = abs(distToIntersect)*0.2;
				distScale = abs(distScale-2);
				//distScale = max(distScale,1);
				shadowWidth *= distScale*distScale;
				shadowWidth += 0.1;
				postRad -= 0.2;
				//postRad = postRad/max(0.4,distScale*distScale) * 0.1;
				columnShadow = saturate((columnShadow - postRad) / shadowWidth);
			}
			
			{
			//-4.76, -13.85
			//-12.47
				float distToPlane = IN.worldPos.z + 12.47;
				float distToIntersect = distToPlane/-R.z;
				distToIntersect = abs(distToIntersect);
				distToIntersect = min(distToIntersect, abs(distToPlane)/(abs(R.x)));
				float3 spherePos = IN.worldPos + R * distToIntersect;
				spherePos.x = min(max(spherePos.x, -13.85+1), -4.76-1);
				spherePos.y = min(max(spherePos.y, 9.24), 18.67);
				spherePos.z = -12.47;
				float3 cr = cross(R, spherePos - IN.worldPos);
				float distFromPointToRay = length(cr);
				
			//	if( dot(R, spherePos - IN.worldPos) > 0 )
			//	distFromPointToRay = 9000;
				
			//	distFromPointToRay = length( spherePos - IN.worldPos );
				
			//	o.Emission = saturate(distFromPointToRay-2);
			//	o.Emission = (distToIntersect);
			
				frontShadow = saturate(distFromPointToRay-1);
			}
			#endif
			
			
			{
			//R.y = max(0.01,R.y); R = normalize(R);
				float distToIntersect = (18.67 - IN.worldPos.y) / R.y;
				float distToIntersect2 = (16.65 - IN.worldPos.y) / R.y;
				//distToIntersect = saturate(distToIntersect);
				float3 intersect = IN.worldPos + R * distToIntersect;
				float3 intersect2 = IN.worldPos + R * distToIntersect2;
				
				float2 center = float2(-9.305, -22.73);
				float2 hw = float2(3.37, 10.42)/2;
					intersect.xz -= center;
					intersect.xz = max( -hw, min( hw, intersect.xz ) );
					intersect.xz += center;
					
				float3 SkyL = intersect - IN.worldPos;
					
				float atten = 3;
				atten *= saturate(R.y);
				atten *= inFrontArea;
				
				float shadow1 = saturate((intersect2.x - (-16.04+.5))*1);
				float shadow2 = saturate((intersect2.x - (-2.57+.5))*-1);
				
				atten *= R.x > 0 ? shadow1 : shadow2;
				
				o.Emission = DoLight(o, normalize(SkyL), normalize(IN.worldViewDir), atten, float3(149,161,236*.9)/255, N).rgb;
			
		//	o.Emission = normalize(SkyL)*0.5+0.5;
			}
			
		//	o.Emission = R*0.5+0.5;
		
			float specFade = 1-saturate( (abs(IN.worldPos.x + 9.305)-(6.735-0.5)) );//2.57 , 16.04
			
			float lx = R.x < 0 ? -20.08+0.1 : 1.47-0.1;
			float3 L0 = float3(lx, 16.82, -34.03+3) - IN.worldPos;
			float3 L1 = float3(lx, 16.82, -8.43) - IN.worldPos;
			float3 Ld = L1 - L0;
			
			float t = (dot(R,L0)*dot(R,Ld) - dot(L0,Ld))
					/ (dot(Ld,Ld) - dot(R,Ld)*dot(R,Ld));
					
					
		//	t = (dot(N,L0)*dot(N,Ld) - dot(L0,Ld))
		//		/ (dot(Ld,Ld) - dot(N,Ld)*dot(N,Ld));
		
			t = saturate(t);
			float3 L = L0 + t*Ld;
		
			float atten = length(L);
			atten = 5/(atten);
			atten = saturate(atten)*2;
			
			atten *= saturate(abs(R.x)*3);
			//atten = 1;
			
			//atten *= h;
			//atten *= columnShadow*inFrontArea;
			
			L = normalize(L);
			
			atten *= saturate(L.y*2-0.25);
			
			
			//o.Emission = 1-shadow;
			o.Emission += DoLight(o, L, normalize(IN.worldViewDir), atten*2, float3(.8,.9,1), N).rgb;
		
		//o.Emission = normalize(L1)*0.5+0.5;
		
		//	o.Gloss = 1;
		//	o.Specular = 1;
		//	_SpecColor = float4(specTex.rrr,0);
			
			/*o.Albedo = 0;
			o.Specular = 0;
			o.Gloss = 0;
			_SpecColor = 0;*/
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
