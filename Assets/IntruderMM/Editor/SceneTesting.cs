﻿using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Assets.IntruderMM.Editor
{
    public class SceneTesting : MonoBehaviour
    {
        [MenuItem("Intruder/Play Scene in Intruder _%i")]
        public static void CheckNameAndPlayInIntruder()
        {
            if (EditorSceneManager.GetActiveScene().name == "")
            {
                UnityEngine.Debug.LogError("Error launching Intruder, please save and name your scene.");
            }
            var intruderApplicationPath = GetIntruderPath();
            if (intruderApplicationPath == null)
            {
                print("Launch of Intruder canceled");
                return;
            }
            
            print("About to launch " + intruderApplicationPath);
            LaunchApplication(intruderApplicationPath);
        }

        [MenuItem("Intruder/Select Intruder Application")]
        public static string FindIntruderApplication()
        {
            var intruderApplicationPath = string.Empty;
#if UNITY_EDITOR_OSX
		    intruderApplicationPath = EditorUtility.OpenFilePanel("Find Intruder Application", "", "app");
#endif
#if UNITY_EDITOR_WIN
            intruderApplicationPath = EditorUtility.OpenFilePanel("Find Intruder Application", "", "exe");
#endif
            PlayerPrefs.SetString("ExportLevel.intruderApplicationPath", intruderApplicationPath);

            var intruderWorkingDirectory = Path.GetDirectoryName(intruderApplicationPath);
            
            PlayerPrefs.SetString("ExportLevel.intruderWorkingDirectory", Path.GetFullPath(intruderWorkingDirectory));
            PlayerPrefs.SetString("ExportLevel.intruderLevelDirectory", Path.Combine(intruderWorkingDirectory ,"/content/levels"));
            PlayerPrefs.Save();

            return intruderApplicationPath;
        }

        private static string GetIntruderPath()
        {
            var intruderApplicationPath = string.Empty;

            if (PlayerPrefs.HasKey("ExportLevel.intruderApplicationPath"))
            {
                intruderApplicationPath = PlayerPrefs.GetString("ExportLevel.intruderApplicationPath");

#if UNITY_EDITOR_OSX
		if (!System.IO.Directory.Exists(intruderApplicationPath)) {
#endif
#if UNITY_EDITOR_WIN
                if (!File.Exists(intruderApplicationPath))
                {
#endif
                    // The Intruder application appears to exist
                    print(intruderApplicationPath + " file doesn't appear to exist.");
                    PlayerPrefs.DeleteKey("ExportLevel.intruderApplicationPath");
                    PlayerPrefs.Save();
                    intruderApplicationPath = string.Empty;
                }
            }

            if (intruderApplicationPath != string.Empty) return intruderApplicationPath;
            
            intruderApplicationPath = FindIntruderApplication();
            return intruderApplicationPath != string.Empty ? intruderApplicationPath : null;
        }

        private static void LaunchApplication(string intruderApplicationPath)
        {
            // Make sure this scene file is exported
            if (!ExportLevel.ExportSceneForIntruder())
            {
                return;
            }
        
            var lastLevelExportFullPath = PlayerPrefs.GetString("ExportLevel.lastLevelExportFullPath");

            var myProc = new Process();
            myProc.StartInfo.FileName = intruderApplicationPath;
#if UNITY_EDITOR_OSX //myProc.StartInfo.Arguments = --args loadlevel ' + lastLevelNameNoExtension + '';

	myProc.StartInfo.Arguments = "--args loadlevel \"" + lastLevelExportFullPath + "m" + "\"";
#endif
#if UNITY_EDITOR_WIN
            myProc.StartInfo.Arguments = "loadlevel \"" + lastLevelExportFullPath + "w" + "\"";
#endif
            myProc.Start();

            print("Argments: " + myProc.StartInfo.Arguments);
        }
    }
}