﻿using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class VersionCheck
{
	private const string MMVersion = "2.00";
	public static string unityVersion;
	const string versionCheckAddress = "https://docs.google.com/document/d/16S0yPjfs0zo1eRXYmybQcQqzMO-A4Pk5HgvuAPaWOV0/export?format=txt";
	public static bool approved = false;
	public static WWW versionWWW;
	public static bool read;
	public static bool startedRead = false;
	public static int timeOut = 0;

	static VersionCheck()
	{
		Start();
	}

	static void CheckColorSpace()
	{
		if (PlayerSettings.colorSpace != ColorSpace.Linear)
		{
			Debug.Log("Setting color space to Linear");
			PlayerSettings.colorSpace = ColorSpace.Linear;
		}
	}

	static void Start()
	{

		if (!startedRead)
		{
			unityVersion = Application.unityVersion;

			versionWWW = new WWW(versionCheckAddress);
			EditorApplication.update += Update;
			startedRead = true;
		}

		CheckColorSpace();
	}

	static void Compiling()
	{
		if (EditorApplication.isCompiling)
		{
			Debug.Log("Still compiling!");
		}
	}

	static void Update()
	{
		if (!startedRead)
		{
			Debug.Log("Checking versions!");
		}

		if (versionWWW.isDone && !read)
		{
			if (versionWWW.error == null)
			{
				read = true;
				string myVersionText = versionWWW.text;

				CheckIfVersionIsCorrect(myVersionText);

				EditorApplication.update -= Update;
			}
			else
			{
				read = true;
				Debug.LogError("ERROR: Download error. Couldn't get Intruder MM Version");
				EditorApplication.update -= Update;

			}

		}

		timeOut++;

		if (timeOut > 1200)
		{
			Debug.LogError("ERROR: Timed out checking versions, make sure your Intruder MM and Unity versions are correct!");
			EditorApplication.update -= Update;
		}
	}

	static void CheckIfVersionIsCorrect(string myVersionText)
	{
		// Debug.Log(myVersionText);
		string[] versionSplit = myVersionText.Split(";" [0]);
		bool err = false;

		if (versionSplit.Length < 2)
		{
			Debug.LogError("Error online MM reading version, check to make sure they are correct and you are connected to the internet!");
			return;
		}

		if (versionSplit[2] != null)
			Debug.Log(versionSplit[2]);

		Debug.Log("Unity version match? " + unityVersion + " == " + versionSplit[1] + "   " + StringEquals(unityVersion, versionSplit[1]));

		if (!StringEquals(unityVersion, versionSplit[1]))
		{
			Debug.LogError("ERROR: WRONG UNITY VERSION, Please use Unity version " + versionSplit[1]);
			err = true;
		}

		if (!Assets.IntruderMM.Editor.ExportLevel.HasAllUnityBuildPlatforms())
		{
			err = true;
		}

		Debug.Log("IntruderMM version match? " + MMVersion + " == " + versionSplit[0] + "   " + StringEquals(MMVersion, versionSplit[0]));

		if (!StringEquals(MMVersion, versionSplit[0]))
		{
			Debug.LogError("ERROR: WRONG INTRUDER MM VERSION, Please get an update to your Intruder MM! It should be version " + versionSplit[0]);
			err = true;
		}

		if (err)
			return;

		approved = true;
		Debug.Log("Your IntruderMM and Unity3d versions are correct! >:D ");
	}

	static bool StringEquals(string a, string b)
	{
		return string.Equals(a, b, System.StringComparison.InvariantCultureIgnoreCase);
	}
}