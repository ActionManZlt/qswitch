﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/EyeAdaptation" {
	Properties {
	//	_MainTex ("Base (RGB)", 2D) = "white" {}
		_FadeParams ("Fade Params", Vector) = (1,2,0.2,0.1)
		_FadeColor ("Fade Color", Color) = (1,1,1,1)
	}
    SubShader {
	Tags {
	"Queue" = "AlphaTest+1"
	"ForceNoShadowCasting" = "True"
	}
	GrabPass { "_GrabTexture" }

        Pass {
			ZWrite Off
			Cull Back
			Blend SrcAlpha OneMinusSrcAlpha
			
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			//#pragma target 3.0

            #include "UnityCG.cginc"

            struct vertOut {
                float4 pos:SV_POSITION;
                float4 vpos:TEXCOORD1;
                float4 scrPos:TEXCOORD0;
            };
			
		//	uniform sampler2D _CameraDepthTexture;
			float4 _GrabTexture_TexelSize;
			sampler2D _GrabTexture;
			float4 _FadeParams;
			float4 _FadeColor;

            vertOut vert(appdata_base v) {
                vertOut o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.vpos = mul(UNITY_MATRIX_MV, v.vertex);
             //   o.scrPos = ComputeScreenPos(o.pos);
                o.scrPos = ComputeGrabScreenPos(o.pos);

                return o;
            }

            fixed4 frag(vertOut i) : COLOR0 {
                float2 wcoord = (i.scrPos.xy/i.scrPos.w);
				
				float distance = -i.vpos.z;
				
				float fraction = saturate(distance*_FadeParams.z-_FadeParams.w);
				
				float multiplier = (fraction)*_FadeParams.y;
				
				//#if UNITY_UV_STARTS_AT_TOP
				//	wcoord.y = 1-wcoord.y;
				//if (_GrabTexture_TexelSize.y < 0)
				//	wcoord.y = 1-wcoord.y;
				//#endif
				
			//	float depth = Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
				
				float3 col =  tex2D(_GrabTexture,wcoord).rgb;
				
				float3 colmul = multiplier * _FadeColor.rgb;
				
			//	col *= (multiplier+1)*lerp(1,_FadeColor.rgb,fraction);
			//	col *= (colmul+1);
				
			//	multiplier = multiplier*0.5+1;
				col = pow(col, 1/(colmul+1));
				
			//	col.r = frac(depth*256);
			//	col.g = frac(depth*256*256);
			//	col.b = frac(depth*256*256*256);
				
				return float4(col,saturate(fraction*10));
            }

            ENDCG
        }
    }
}
