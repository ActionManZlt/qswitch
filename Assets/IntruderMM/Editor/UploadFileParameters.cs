﻿namespace Assets.IntruderMM.Editor
{
    public class UploadFileParameters
    {
        public string Title;
        public string ChangeNotes;
        public string Description;
        public string LocalPath;
        public string previewImage;
        public string itemId;
        public SteamworksWrapper.WorkshopItem.Visibility visibility;
        public static string[] PossibleTags = {"Hack", "Raid", "In-Dev", "Training", "Silly", "Custom Tuning", "Custom Mission"};
    }
}
