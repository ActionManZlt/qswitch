﻿Shader "QSwitch/Standard" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_SpecTex ("Specular", 2D) = "white" {}
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		//#pragma surface surf BlinnPhong fullforwardshadows dualforward
		//#pragma surface surf Q
		//#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _SpecTex;
		
#if 0
		float3 tonemap( float3 light ) 
		{
			float3 result;

			float crush = 0.0;
			float frange = 13;
			float exposure = 300;
			
			result = log2(1+light*exposure);
			result = smoothstep(crush, frange, result);

		//	result = pow( result, 1/2.2 );

			return result;
		}
		
		half4 LightingQ_SingleLightmap (SurfaceOutput s, fixed4 color, half3 viewDir)
		{
			return color;
		}
		/*half4 LightingQ_DualLightmap (SurfaceOutput s, fixed4 totalColor, fixed4 indirectOnlyColor, half indirectFade, half3 viewDir)
		{
			return totalColor;
		}*/
		half4 LightingQ_DirLightmap (SurfaceOutput s, fixed4 color, fixed4 scale, half3 viewDir, bool surfFuncWritesNormal, out half3 specColor)
		{
			UNITY_DIRBASIS
			half3 scalePerBasisVector;
			
			half3 lm = DirLightmapDiffuse (unity_DirBasis, color, scale, s.Normal, surfFuncWritesNormal, scalePerBasisVector);
			
			half3 lightDir = normalize (scalePerBasisVector.x * unity_DirBasis[0] + scalePerBasisVector.y * unity_DirBasis[1] + scalePerBasisVector.z * unity_DirBasis[2]);
			half3 h = normalize (lightDir + viewDir);

			float nh = max (0, dot (s.Normal, h));
			float spec = pow (nh, s.Specular * 128.0);
			
			// specColor used outside in the forward path, compiled out in prepass
			specColor = lm * _SpecColor.rgb * s.Gloss * spec;
			
			// spec from the alpha component is used to calculate specular
			// in the Lighting*_Prepass function, it's not used in forward
			return half4(lm, spec);
		}
		half4 LightingQ (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{
			half3 h = normalize (lightDir + viewDir);

			float3 N = s.Normal;
			half diff = saturate(dot (N, lightDir)*0.5+0.5);

			float nh = saturate( dot (N, h));
			float gloss = pow(2,s.Gloss*13);
			float spec = pow(nh, gloss)* _SpecColor.rgb;
			//spec *= (gloss+1) * 2;

			half4 c;
			c.rgb = _LightColor0.rgb * (spec + s.Albedo) * diff * atten * 1;
			c.a = 1;
			
			//c.rgb = c.rgb / (c.rgb+10);
			//c.rgb = tonemap(c.rgb);
			return c;
		}
		half4 LightingQ_PrePass (SurfaceOutput s, half4 light)
		{
			fixed spec = light.a * s.Gloss;
			
			fixed4 c;
			c.rgb = (s.Albedo * light.rgb + light.rgb * _SpecColor.rgb * spec);
			c.a = s.Alpha + spec * _SpecColor.a;
			return c;
		}
#endif
		
		struct Input {
			float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {			
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 specTex = tex2D (_SpecTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Specular = 1;//pecTex.r * 0.06;
			o.Gloss = (specTex.g);
			_SpecColor.rgb = lerp( o.Albedo.rgb, 0.5*specTex.rrr, specTex.bbb );
			//_SpecColor = float4(specTex.rrr*0.1,0);
		}
		ENDCG
	}
	
	FallBack "Specular"
}
