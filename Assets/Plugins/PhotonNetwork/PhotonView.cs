﻿using System;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;
using ExitGames.Client.Photon;

public class PhotonView : Photon.MonoBehaviour
{
	[SerializeField]
    private int viewIdField = 6103;

    public int viewID
    {
        get { return this.viewIdField; }
        set
        {
            this.viewIdField = value;
        }
    }
}