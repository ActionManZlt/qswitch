﻿Shader "QSwitch/SecurityMonitor" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TitleTex ("Base (RGB)", 2D) = "white" {}
		_BarrelPower ("BarrelPower", Float ) = 1
		_Damage ("Damage", Float ) = 0
		_TextOffset("TextOffset", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
	//	#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _TitleTex;
		float _Damage;
		float _TextOffset;

		struct Input {
			float2 uv_MainTex;
		};
		
		const float PI = 3.1415926535;
		uniform float _BarrelPower; 
		float2 Distort(float2 p)
		{
			p = p*2-1;
		    
		    float l = length(p);
		    p /= l;
		    l = pow(l,_BarrelPower);
		    p*= l;
		    return p*0.5+0.5;
		}

		void surf (Input IN, inout SurfaceOutput o) {
		
			float2 uv = IN.uv_MainTex;
			float f = frac(uv.y*128 + _Time.x*10);
			float f2 = frac(uv.x*256);
			
			
			float2 offset = uv * float2(320,256)/2 - float2(15,256-15)/2;
			offset = round(offset);
			float dist = length(offset);
			dist = 1-saturate(dist-5);
			dist = frac(_Time.x*20.0)<0.5 ? dist : 0;
			
			
			f = abs(f*2-1);
			f2 = abs(f2*2-1);
			
			f = f*0.5+.75;
			f2 = f2*0.0625+0.9375;
			
			IN.uv_MainTex += float2(-1.0/256, 1.0/128)*f;
			
			IN.uv_MainTex = Distort(IN.uv_MainTex);
			
			float2 px = float2(1.0/320, 1.0/256); 
			
			half4 c = //tex2D (_MainTex, IN.uv_MainTex)
			        + tex2D (_MainTex, IN.uv_MainTex + float2( 1, 1)*px)
			        + tex2D (_MainTex, IN.uv_MainTex + float2(-1, 1)*px)
			        + tex2D (_MainTex, IN.uv_MainTex + float2( 1,-1)*px)
			        + tex2D (_MainTex, IN.uv_MainTex + float2(-1,-1)*px);
					
			float2 textUv = uv*10-9;
			float2 textUvTest = saturate(textUv - float2(0,0.5));
			float textAlpha = dot( textUvTest, textUvTest.yx ) > 0 ? 1 : 0;
			textUv.y -= _TextOffset;
			half4 text = tex2D(_TitleTex, textUv);
			
			c.rgb += text.rgb * textAlpha;
			        
			c.rgb = lerp(c.rrr, half3(1,0,0), dist*0.75);
			half3 color =  c.rgb * f * 0.25 * f2;
			o.Emission = color;
		//	o.Albedo = c.rgb;
		//	o.Alpha = c.a;
		}
		ENDCG
	} 
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 2.0

		sampler2D _MainTex;
		sampler2D _TitleTex;
		float _Damage;
		float _TextOffset;

		struct Input {
			float2 uv_MainTex;
		};
		
		const float PI = 3.1415926535;
		uniform float _BarrelPower;
		float2 Distort(float2 p)
		{
			p = p*2-1;
		    
		    float l = length(p);
		    p /= l;
		    l = pow(l,_BarrelPower);
		    p*= l;
		    return p*0.5+0.5;
		}

		void surf (Input IN, inout SurfaceOutput o) {
		
			float2 uv = IN.uv_MainTex;
			float f = frac(uv.y*128 + _Time.x*10);
			float f2 = frac(uv.x*256);
			
	
			f = abs(f*2-1);
			f2 = abs(f2*2-1);
			
			f = f*0.5+.75;
			f2 = f2*0.0625+0.9375;
			
			IN.uv_MainTex += float2(-1.0/256, 1.0/128)*f;
			
			IN.uv_MainTex = Distort(IN.uv_MainTex);
			
			float2 px = float2(1.0/320, 1.0/256); 
			
			half4 c = //tex2D (_MainTex, IN.uv_MainTex)
			        + tex2D (_MainTex, IN.uv_MainTex + float2( 1, 1)*px)
			        + tex2D (_MainTex, IN.uv_MainTex + float2(-1, 1)*px)
			        + tex2D (_MainTex, IN.uv_MainTex + float2( 1,-1)*px)
			        + tex2D (_MainTex, IN.uv_MainTex + float2(-1,-1)*px);
					
			        
			c.rgb = c.rrr;
			half3 color =  c.rgb * f * 0.25 * f2;
			o.Emission = color;
		//	o.Albedo = c.rgb;
		//	o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Self-Illum/Diffuse"
}
