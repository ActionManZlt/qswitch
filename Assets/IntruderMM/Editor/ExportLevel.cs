﻿// This script is used to export a Unity scene file into an Intruder Level File .ilf
// We now make .ilfw files for Windows and .ilfm files for Mac
using System;
using System.Collections.Generic;
using System.IO;
using Steamworks;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

//INTRUDER MM VERSION 1
namespace Assets.IntruderMM.Editor
{
    public class ExportLevel : MonoBehaviour
    {
        private static readonly string ExportBaseDir = Path.Combine(Application.dataPath, Path.Combine("..", "Exports"));

        static bool uploading;
        public static int uploadcount = 0;
        public static SteamworksWrapper wrapper;
        private static UploadFileParameters parameters;
        private static UploadWindow _uploadWindowInstance;

        [MenuItem("Intruder/Map Upload _%u")]
        public static void LoadUploadWindow()
        {
            if (UploadWindow.Instance != null)
            {
                _uploadWindowInstance = UploadWindow.Instance;
                _uploadWindowInstance.Focus();
            }

            if (_uploadWindowInstance != null)
                return;
            // Get existing open window or if none, make a new one:	
            _uploadWindowInstance = ScriptableObject.CreateInstance<UploadWindow>();
            _uploadWindowInstance.titleContent = new GUIContent("Map Upload");
            _uploadWindowInstance.minSize = new Vector2(550, 600);
            _uploadWindowInstance.Show();
        }

        public static bool HasAllUnityBuildPlatforms()
        {
            var moduleManager = System.Type.GetType("UnityEditor.Modules.ModuleManager,UnityEditor.dll");
            var isPlatformSupportLoaded = moduleManager.GetMethod("IsPlatformSupportLoaded", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            var getTargetStringFromBuildTarget = moduleManager.GetMethod("GetTargetStringFromBuildTarget", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            bool windowsLoaded = (bool) isPlatformSupportLoaded.Invoke(null, new object[]
            {
                (string) getTargetStringFromBuildTarget.Invoke(null, new object[] { BuildTarget.StandaloneWindows64 })
            });

            bool osxLoaded = (bool) isPlatformSupportLoaded.Invoke(null, new object[]
            {
                (string) getTargetStringFromBuildTarget.Invoke(null, new object[] { BuildTarget.StandaloneOSXIntel64 })
            });

            if (!(windowsLoaded && osxLoaded))
                Debug.LogError("Error: Cannot build. Make sure both Windows and OSX build platforms are installed. Run Unity installer again to fix.");

            return windowsLoaded && osxLoaded;
        }

        //[MenuItem("Intruder/Export Scene for Intruder _%e")]
        public static bool ExportSceneForIntruder()
        {
            return ExportSceneForIntruderAs();
        }

        public static bool ExportSceneForIntruderAs()
        {
            if (!HasAllUnityBuildPlatforms())
            {
                return false;
            }

            if (!Directory.Exists(ExportBaseDir))
            {
                Directory.CreateDirectory(ExportBaseDir);
            }

            //clear out the staging dir
            Array.ForEach(Directory.GetFiles(ExportBaseDir), File.Delete);
            Array.ForEach(new DirectoryInfo(ExportBaseDir).GetDirectories(), info => info.Delete(true));

            return MultiPlatformExport(Path.Combine(ExportBaseDir, "map.ilf"));
        }

        private static bool MultiPlatformExport(string myPath)
        {
            if (!DoWorkToExportScene(myPath + "w", 0)) return false;
            if (!DoWorkToExportScene(myPath + "m", 1)) return false;
            PlayerPrefs.SetString("ExportLevel.lastLevelExportFullPath", myPath);
            return true;
        }

        // This is where the actual work is done
        private static bool DoWorkToExportScene(string myPath, int platIndex)
        {
            if (SceneManager.GetActiveScene().name == "")
            {
                Debug.LogError("Error exporting scene! Please name and save your scene.");
                return false;
            }

            const string defaultLevelName = "Assets/Level1.unity";
            const string defaultLevelBackupName = "Assets/Level1.unity_backup";

            if (string.IsNullOrEmpty(myPath))
            {
                print("Export canceled");
                return false;
            }

            //////////////CLEAN LEVEL
            CleanLevel();

            var levelPath = EditorSceneManager.GetActiveScene().path;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            if (!File.Exists(defaultLevelName))
            {
                var scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);
                EditorSceneManager.SaveScene(scene, defaultLevelName);
                EditorSceneManager.CloseScene(EditorSceneManager.GetActiveScene(), false);
                EditorSceneManager.OpenScene(levelPath);
            }

            if (SceneManager.GetActiveScene().name != defaultLevelName)
            {
                if (File.Exists(defaultLevelName))
                {
                    FileUtil.ReplaceFile(defaultLevelName, defaultLevelBackupName);
                }

                FileUtil.ReplaceFile(SceneManager.GetActiveScene().path, defaultLevelName);
            }

            PlayerPrefs.SetString("ExportLevel.lastLevelSceneFilePath", SceneManager.GetActiveScene().path);

            var levelFileName = Path.GetFileName(myPath);
            PlayerPrefs.SetString("ExportLevel.lastLevelExportFileName", levelFileName);
            PlayerPrefs.SetString("ExportLevel.lastLevelExportDirectory", Path.GetDirectoryName(myPath));

            print("Exporting " + SceneManager.GetActiveScene().name + " to " + myPath);
            var crcResult = 0;

            var names = AssetDatabase.GetAllAssetBundleNames();

            foreach (var name in names)
            {
                print("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }

            var assetImporter = AssetImporter.GetAtPath(defaultLevelName);
            assetImporter.assetBundleName = levelFileName;

            switch (platIndex)
            {
                case 0:
                    BuildPipeline.BuildAssetBundles(ExportBaseDir,
                        BuildAssetBundleOptions.ForceRebuildAssetBundle,
                        BuildTarget.StandaloneWindows);
                    break;
                case 1:
                    BuildPipeline.BuildAssetBundles(ExportBaseDir,
                        BuildAssetBundleOptions.ForceRebuildAssetBundle,
                        BuildTarget.StandaloneOSXIntel);
                    break;
            }

            // Remember the crc for this level that you exported - blaze
            PlayerPrefs.SetInt("ExportLevel.crc." + PlayerPrefs.GetString("ExportLevel.lastLevelExportFullPath"),
                crcResult);

            if (SceneManager.GetActiveScene().name != defaultLevelName && File.Exists(defaultLevelBackupName))
            {
                FileUtil.ReplaceFile(defaultLevelBackupName, defaultLevelName);
            }

            var go = FindObjectOfType<CustomLevelSettings>();

            if (go != null)
            {
                DestroyImmediate(go.gameObject);
            }

            print("Exporting complete!");
            PlayerPrefs.Save();

            names = AssetDatabase.GetAllAssetBundleNames();

            foreach (var name in names)
            {
                print("Asset Bundle: " + name);
                AssetDatabase.RemoveAssetBundleName(name, true);
            }
            return true;
        }

        private static void CleanLevel()
        {
            var customLevelSettings = FindObjectOfType<CustomLevelSettings>();
            if (customLevelSettings != null)
            {
                customLevelSettings.SetSettings();
            }
            else
            {
                var customLevelSettingsGameObject = new GameObject { name = "CustomLevelSettings" };
                customLevelSettings = customLevelSettingsGameObject.AddComponent<CustomLevelSettings>();
                customLevelSettings.SetSettings();
            }

            var cameras = FindObjectsOfType<Camera>();
            var cameraCount = 0;

            foreach (var camera in cameras)
            {
                if (camera.gameObject.GetComponent<ObserveCamProxy>() != null ||
                    !camera.enabled || camera.targetTexture != null) continue;
                camera.enabled = false;
                cameraCount++;
            }

            if (cameraCount > 0)
            {
                print("You had " + cameraCount + " extra Cameras enabled, they were disabled for export.");
            }

            var screens = FindObjectsOfType<SecurityScreen>();
            foreach (var securityScreen in screens)
            {
                securityScreen.gameObject.SetActive(false);
            }

            var listeners = FindObjectsOfType<AudioListener>();
            var listenerCount = 0;

            foreach (var audioListener in listeners)
            {
                if (!audioListener.enabled) continue;
                audioListener.enabled = false;
                listenerCount++;
            }

            if (listenerCount > 0)
                print("You had " + listenerCount + " extra AudioListeners enabled, they were disabled for export.");
        }

        public static void Upload()
        {
            parameters = new UploadFileParameters { LocalPath = ExportBaseDir };

            if (EditorApplication.isCompiling)
            {
                print("Please wait until compiling finishes before uploading. Should be done in a few seconds.");
                uploadcount = 5;
                return;
            }

            if (uploading)
            {
                print("Still uploading other file... please wait.");
            }

            if (!PlayerPrefs.HasKey("ExportLevel.mapname"))
            {
                print("Map name not found! Try naming again!");
                uploadcount = 5;
                return;
            }
            parameters.Title = PlayerPrefs.GetString("ExportLevel.mapname");

            // if (!PlayerPrefs.HasKey("ExportLevel.description"))
            // {
            //     print("Map description not found! Try entering a description again!");
            //     uploadcount = 5;
            //     return;
            // }
            // parameters.Description = PlayerPrefs.GetString("ExportLevel.description");

            if (!PlayerPrefs.HasKey("ExportLevel.changeNotes"))
            {
                print("Map change notes not found! Try entering change notes again!");
                uploadcount = 5;
                return;
            }
            parameters.ChangeNotes = PlayerPrefs.GetString("ExportLevel.changeNotes");

            if (!PlayerPrefs.HasKey("ExportLevel.visibility"))
            {
                print("Visibily not set correctly, change visibility and try again!");
                uploadcount = 5;
                return;
            }
            parameters.visibility = (SteamworksWrapper.WorkshopItem.Visibility)
            Enum.Parse(typeof(SteamworksWrapper.WorkshopItem.Visibility),
                PlayerPrefs.GetString("ExportLevel.visibility"), true);

            if (!PlayerPrefs.HasKey("ExportLevel.previewImage"))
            {
                print("Map preview image not found! Try selecting a preview image again!");
                uploadcount = 5;
                return;
            }
            parameters.previewImage = PlayerPrefs.GetString("ExportLevel.previewImage");
            if (!File.Exists(parameters.previewImage))
            {
                print("Preview image not fount! Try selecting a preview image again!");
                uploadcount = 5;
                return;
            }

            if (PlayerPrefs.HasKey("ExportLevel.itemId"))
            {
                parameters.itemId = PlayerPrefs.GetString("ExportLevel.itemId");
            }

            if (!Directory.Exists(ExportBaseDir))
            {
                UnityEngine.Debug.LogError("ERROR: Could not find export directory: " + ExportBaseDir);
                uploadcount = 5;
                return;
            }

            if (!File.Exists(Path.Combine(ExportBaseDir, "map.ilfw")))
            {
                UnityEngine.Debug.LogError("ERROR: Could not find .ilfw file in expected location: " + ExportBaseDir);
                uploadcount = 5;
                return;
            }

            if (!File.Exists(Path.Combine(ExportBaseDir, "map.ilfw")))
            {
                UnityEngine.Debug.LogError("ERROR: Could not find .ilfm file in expected location: " + ExportBaseDir);
                uploadcount = 5;
                return;
            }

            UploadFileToSteam();
        }

        public static void UploadFileToSteam()
        {
            EnsureWrapperInitialization();

            uploading = true;
            if (!string.IsNullOrEmpty(parameters.itemId))
            {
                try
                {
                    wrapper.SetCurrentItemId(Convert.ToUInt64(parameters.itemId));
                }
                catch
                {
                    print("The Workshop Item ID was not in the correct format.  Please modify and try again.");
                    return;
                }
                print("Updating existing Item!");
                wrapper.currentItem.title = parameters.Title;
                // wrapper.currentItem.description = parameters.Description;
                wrapper.currentItem.previewImagePath = parameters.previewImage;
                wrapper.currentItem.visibility = parameters.visibility;
                wrapper.SubmitCurrentItem(parameters.LocalPath, parameters.ChangeNotes);
            }
            else
                wrapper.CreateWorkshopItem();

        }

        public static void UpdateTags(bool[] tags)
        {
            if (tags == null)
            {
                UnityEngine.Debug.LogError("error: tags is null in UpdateTags!");
                return;
            }

            EnsureWrapperInitialization();

            uploading = true;

            if (parameters == null)
                parameters = new UploadFileParameters { LocalPath = ExportBaseDir };

            parameters.itemId = PlayerPrefs.GetString("ExportLevel.itemId");
            print("Trying to update tags, item id:" + parameters.itemId);

            if (!string.IsNullOrEmpty(parameters.itemId))
            {
                try
                {
                    wrapper.SetCurrentItemId(Convert.ToUInt64(parameters.itemId));
                }
                catch
                {
                    print("The Workshop Item ID was not in the correct format.  Please modify and try again.");
                    return;
                }
                print("Creating tags list for " + parameters.itemId);

                List<string> tagList = new List<string>();

                for (int i = 0; i < tags.Length; i++)
                {
                    if (tags[i])
                        tagList.Add(UploadFileParameters.PossibleTags[i]);
                }

                print("Updating item tags for " + parameters.itemId);
                wrapper.UpdateTags(tagList);
            }
        }

        public static void EnsureWrapperInitialization()
        {
            if (wrapper == null)
                wrapper = new SteamworksWrapper();

            if (wrapper.isInitialized) return;
            wrapper.Initialize();

            wrapper.OnSubmitItemDone = ok =>
            {
                if (!ok)
                {
                    UnityEngine.Debug.LogError("Uh Oh! Something went wrong with the upload.  Please try again.");
                    EditorUtility.ClearProgressBar();

                    if (UploadWindow.Instance != null)
                        UploadWindow.Instance.Close();

                    return;
                }

                print("Upload complete!");
                uploadcount++;
                uploading = false;
            };

            wrapper.OnCreateItemDone = (ok, id) =>
            {
                print("Item Created!");
                PlayerPrefs.SetString("ExportLevel.itemId", id.m_PublishedFileId.ToString());
                wrapper.currentItem.title = parameters.Title;
                // wrapper.currentItem.description = parameters.Description;
                wrapper.currentItem.previewImagePath = parameters.previewImage;
                wrapper.currentItem.visibility = parameters.visibility;
                wrapper.SubmitCurrentItem(parameters.LocalPath, parameters.ChangeNotes);
                SubscribeToUpload(id);
            };
        }
        public static void SubscribeToUpload(PublishedFileId_t itemId)
        {
            //check for subscription
            var steamId = itemId;
            var itemState = (EItemState) wrapper.GetItemState(steamId);
            var subscribed = (itemState & EItemState.k_EItemStateSubscribed) != 0;

            //if not subscribed, subscribe
            if (!subscribed)
            {
                UnityEngine.Debug.Log("Item not subscribed to, subscribing.");
                wrapper.SubscribeToItem(steamId);
            }
        }
    }
}