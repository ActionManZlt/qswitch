﻿using System;
using System.Collections.Generic;
using Steamworks;
using UnityEngine;

public class SteamworksWrapper
{

    public const uint APP_ID = 518150;
    public const string WORKSHOP_TERMS_OF_SERVICE_URL = "http://steamcommunity.com/sharedfiles/workshoplegalagreement";
    public const string COMMUNITY_FILE_PAGE_URL = "steam://url/CommunityFilePage/";

    public bool isInitialized;

    protected CallResult<CreateItemResult_t> createItemResult;
    protected CallResult<SubmitItemUpdateResult_t> submitItemResult;
    protected Callback<ItemInstalled_t> itemInstalled;
    protected Callback<RemoteStoragePublishedFileSubscribed_t> remotePublishedFileSubscribed;
    protected Callback<RemoteStoragePublishedFileUnsubscribed_t> remotePublishedFileUnsubscribed;

    public delegate void DelOnStateChanged();
    public delegate void DelOnCreateItemDone(bool ok, PublishedFileId_t itemId);
    public delegate void DelOnSubmitItemDone(bool ok);
    public delegate void DelOnUGCQueryDone(bool ok, UGCQueryResult[] details);
    public delegate void DelOnItemInstalled(PublishedFileId_t itemId);
    public delegate void DelOnRemotePublishedFileSubscribed(PublishedFileId_t itemId);
    public delegate void DelOnRemotePublishedFileUnsubscribed(PublishedFileId_t itemId);

    public DelOnStateChanged OnStateChanged;
    public DelOnCreateItemDone OnCreateItemDone;
    public DelOnSubmitItemDone OnSubmitItemDone;
    public DelOnItemInstalled OnItemInstalled;
    public DelOnRemotePublishedFileSubscribed OnRemotePublishedFileSubscribed;
    public DelOnRemotePublishedFileUnsubscribed OnRemotePublishedFileUnsubscribed;

    public bool needsToAcceptWorkshopLegalAgreement;

    public WorkshopItem currentItem;

    public EResult lastResult;
    public string errorMessage;

    public PublishedFileId_t[] subscribedItems;

    bool isUpdatingItem;
    UGCUpdateHandle_t lastUpdateHandle;

    Dictionary<UGCQueryHandle_t, CallResult<SteamUGCQueryCompleted_t>> queryUGCCallResultHandler;

    public SteamworksWrapper()
    {
        queryUGCCallResultHandler = new Dictionary<UGCQueryHandle_t, CallResult<SteamUGCQueryCompleted_t>>();
    }

    public bool Initialize()
    {
        isInitialized = SteamAPI.Init();

        if (isInitialized)
        {
            RegisterCallbacks();
        }

        SignalStateChanged();

        return isInitialized;
    }

    public void RegisterCallbacks()
    {
        Debug.Log("Registering callbacks");
        createItemResult = CallResult<CreateItemResult_t>.Create(OnCreateItemResult);
        submitItemResult = CallResult<SubmitItemUpdateResult_t>.Create(OnSubmitItemResult);
        itemInstalled = Callback<ItemInstalled_t>.Create(OnItemInstalledInt);
        remotePublishedFileSubscribed = Callback<RemoteStoragePublishedFileSubscribed_t>.Create(OnRemotePublishedFileSubscribedInt);
        remotePublishedFileUnsubscribed = Callback<RemoteStoragePublishedFileUnsubscribed_t>.Create(OnRemotePublishedFileUnsubscribedInt);
    }

    void SignalStateChanged()
    {
        if (OnStateChanged != null)
        {
            OnStateChanged();
        }
    }

    public bool HasCurrentItem()
    {
        return currentItem != null;
    }

    public void Shutdown()
    {
        SteamAPI.Shutdown();

        isInitialized = false;

        SignalStateChanged();
    }

    void CheckInitialized()
    {
        if (!isInitialized)
        {
            throw new Exception("SteamworksWrapper is not initialized.");
        }
    }

    public void Update()
    {
        if (isInitialized)
        {
            SteamAPI.RunCallbacks();
        }
    }

    public string Username()
    {
        CheckInitialized();

        return SteamFriends.GetPersonaName();
    }

    // Callbacks

    void OnItemInstalledInt(ItemInstalled_t p)
    {
        if (p.m_unAppID == SteamUtils.GetAppID())
        {
            if (OnItemInstalled != null)
            {
                OnItemInstalled(p.m_nPublishedFileId);
            }
        }

        SignalStateChanged();
    }

    void OnRemotePublishedFileSubscribedInt(RemoteStoragePublishedFileSubscribed_t p)
    {
        if (p.m_nAppID == SteamUtils.GetAppID())
        {
            if (OnRemotePublishedFileSubscribed != null)
            {
                OnRemotePublishedFileSubscribed(p.m_nPublishedFileId);
            }
        }

        SignalStateChanged();
    }

    void OnRemotePublishedFileUnsubscribedInt(RemoteStoragePublishedFileUnsubscribed_t p)
    {
        if (p.m_nAppID == SteamUtils.GetAppID())
        {
            if (OnRemotePublishedFileUnsubscribed != null)
            {
                OnRemotePublishedFileUnsubscribed(p.m_nPublishedFileId);
            }
        }

        SignalStateChanged();
    }

    // Steam friends

    public CSteamID[] GetAllFriends()
    {
        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);

        if (friendCount < 0)
        {
            throw new Exception("Could not get friend count");
        }

        CSteamID[] friends = new CSteamID[friendCount];

        for (int i = 0; i < friendCount; i++)
        {
            friends[i] = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
        }

        return friends;
    }

    public string GetSteamIdNickName(CSteamID id)
    {
        return SteamFriends.GetFriendPersonaName(id);
    }

    // Create workshop item

    public void CreateWorkshopItem()
    {
        CheckInitialized();

        Debug.Log("Creating item");
        SteamAPICall_t callback = SteamUGC.CreateItem(SteamUtils.GetAppID(), EWorkshopFileType.k_EWorkshopFileTypeCommunity);
        createItemResult.Set(callback);
    }

    void OnCreateItemResult(CreateItemResult_t p, bool ioFailure)
    {

        var ok = true;
        lastResult = p.m_eResult;

        if (ioFailure)
        {
            SetError("Create Item failed, IO Failure!");
            ok = false;
        }
        else if (p.m_eResult != EResult.k_EResultOK)
        {
            SetError("Create Item failed, error: " + p.m_eResult);
            ok = false;
        }
        else
        {

            needsToAcceptWorkshopLegalAgreement = p.m_bUserNeedsToAcceptWorkshopLegalAgreement;

            currentItem = new WorkshopItem(p.m_nPublishedFileId);
            currentItem.title = "My new item";
        }

        if (OnCreateItemDone != null)
        {
            OnCreateItemDone(ok, p.m_nPublishedFileId);
        }

        SignalStateChanged();
    }

    // Set workshop item

    public void SetCurrentItemId(ulong id)
    {
        SetCurrentItem(new WorkshopItem(new PublishedFileId_t(id)));
    }

    public void SetCurrentItem(WorkshopItem item)
    {
        CheckInitialized();

        currentItem = item;

        SignalStateChanged();
    }

    // Submit current workshop item

    public void SubmitCurrentItem(string contentPath, string changeNote)
    {
        CheckInitialized();

        if (!HasCurrentItem())
        {
            throw new Exception("Unable to submit item, there is no current item");
        }

        Debug.Log("Submitting item #" + currentItem.itemId.m_PublishedFileId + " from path " + contentPath);

        var handle = SteamUGC.StartItemUpdate(SteamUtils.GetAppID(), currentItem.itemId);

        if (!string.IsNullOrEmpty(currentItem.title))
        {
            SteamUGC.SetItemTitle(handle, currentItem.title);
        }

        // if (!string.IsNullOrEmpty(currentItem.description))
        // {
        //     SteamUGC.SetItemDescription(handle, currentItem.description);
        // }

        if (!string.IsNullOrEmpty(currentItem.previewImagePath))
        {
            SteamUGC.SetItemPreview(handle, currentItem.previewImagePath);
        }

        if (currentItem.visibility != WorkshopItem.Visibility.NoChange)
        {
            ERemoteStoragePublishedFileVisibility visibility;

            switch (currentItem.visibility)
            {
                case WorkshopItem.Visibility.Private:
                    visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityPrivate;
                    break;
                case WorkshopItem.Visibility.FriendsOnly:
                    visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityFriendsOnly;
                    break;
                default:
                    visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityPublic;
                    break;
            }

            SteamUGC.SetItemVisibility(handle, visibility);
        }

        SteamUGC.SetItemContent(handle, contentPath);

        SteamAPICall_t callback = SteamUGC.SubmitItemUpdate(handle, changeNote);
        submitItemResult.Set(callback);

        isUpdatingItem = true;
        lastUpdateHandle = handle;
    }

    public void UpdateTags(List<string> tags)
    {
        CheckInitialized();

        if (!HasCurrentItem())
        {
            throw new Exception("Unable to submit item, there is no current item");
        }

        var handle = SteamUGC.StartItemUpdate(SteamUtils.GetAppID(), currentItem.itemId);

        // SteamUGC.SetItemContent(handle, contentPath);

        SteamUGC.SetItemTags(handle, tags);

        SteamAPICall_t callback = SteamUGC.SubmitItemUpdate(handle, "Tags updated");
        submitItemResult.Set(callback);

        isUpdatingItem = true;
        lastUpdateHandle = handle;
    }

    public bool IsUploadingItem()
    {
        return isUpdatingItem;
    }

    public bool IsPreparingContentUpload()
    {
        ulong processed, total;
        var status = SteamUGC.GetItemUpdateProgress(lastUpdateHandle, out processed, out total);
        return status == EItemUpdateStatus.k_EItemUpdateStatusPreparingContent || status == EItemUpdateStatus.k_EItemUpdateStatusPreparingConfig;
    }

    public float GetUploadProgress()
    {
        ulong processed, total;
        SteamUGC.GetItemUpdateProgress(lastUpdateHandle, out processed, out total);
        return total == 0 ? 0f : Mathf.Clamp01(processed / 1000f / (total / 1000f));
    }

    void OnSubmitItemResult(SubmitItemUpdateResult_t p, bool ioFailure)
    {

        bool ok = true;
        lastResult = p.m_eResult;

        isUpdatingItem = false;

        if (ioFailure)
        {
            SetError("Submit Item failed, IO Failure!");
            ok = false;
        }
        else if (p.m_eResult != EResult.k_EResultOK)
        {
            SetError("Submit Item failed, error: " + p.m_eResult);
            ok = false;
        }
        else
        {
            needsToAcceptWorkshopLegalAgreement = p.m_bUserNeedsToAcceptWorkshopLegalAgreement;
            Debug.Log("Item was successfully uploaded!");
        }

        if (OnSubmitItemDone != null)
        {
            OnSubmitItemDone(ok);
        }

        SignalStateChanged();
    }

    // Steam UGC Query

    public UGCQueryHandle_t CreateUGCQuery(DelOnUGCQueryDone ManagedCallback)
    {
        CheckInitialized();

        var query = SteamUGC.CreateQueryAllUGCRequest(EUGCQuery.k_EUGCQuery_RankedByVotesUp, EUGCMatchingUGCType.k_EUGCMatchingUGCType_Items_ReadyToUse, new AppId_t(APP_ID), new AppId_t(APP_ID), 1);

        CallResult<SteamUGCQueryCompleted_t> callResultHandler = CallResult<SteamUGCQueryCompleted_t>.Create(delegate(SteamUGCQueryCompleted_t p, bool ioFailure)
        {
            OnSteamUGCQueryCompleted(p, ioFailure, ManagedCallback);
        });

        queryUGCCallResultHandler.Add(query, callResultHandler);

        return query;

    }

    public void AddRequiredTagUGCQuery(UGCQueryHandle_t query, string tag)
    {
        SteamUGC.AddRequiredTag(query, tag);
    }

    public void SendUGCQueryRequest(UGCQueryHandle_t query)
    {
        SteamAPICall_t callback = SteamUGC.SendQueryUGCRequest(query);
        queryUGCCallResultHandler[query].Set(callback);

        queryUGCCallResultHandler.Remove(query);
    }

    public void QuickQueryItemInfo(PublishedFileId_t[] fileIds, DelOnUGCQueryDone ManagedCallback)
    {
        CheckInitialized();

        var query = SteamUGC.CreateQueryUGCDetailsRequest(fileIds, (uint) fileIds.Length);

        CallResult<SteamUGCQueryCompleted_t> callResultHandler = CallResult<SteamUGCQueryCompleted_t>.Create(delegate(SteamUGCQueryCompleted_t p, bool ioFailure)
        {
            OnSteamUGCQueryCompleted(p, ioFailure, ManagedCallback);
        });

        SteamAPICall_t callback = SteamUGC.SendQueryUGCRequest(query);
        callResultHandler.Set(callback);
    }

    void OnSteamUGCQueryCompleted(SteamUGCQueryCompleted_t p, bool ioFailure, DelOnUGCQueryDone ManagedCallback)
    {
        bool ok = true;
        lastResult = p.m_eResult;
        UGCQueryResult[] results = null;

        if (ioFailure)
        {
            SetError("Item query failed, IO Failure!");
            ok = false;
        }
        else if (p.m_eResult != EResult.k_EResultOK)
        {
            SetError("Item query failed, error: " + p.m_eResult);
            ok = false;
        }
        else
        {
            results = new UGCQueryResult[p.m_unNumResultsReturned];

            for (uint i = 0; i < p.m_unNumResultsReturned; i++)
            {
                SteamUGC.GetQueryUGCResult(p.m_handle, i, out results[i].details);
                SteamUGC.GetQueryUGCPreviewURL(p.m_handle, i, out results[i].previewImageURL, 1024);
            }
        }

        ManagedCallback(ok, results);

        SignalStateChanged();
    }

    public void SubscribeToItem(PublishedFileId_t fileId)
    {
        SteamUGC.SubscribeItem(fileId);
    }

    public uint GetItemState(PublishedFileId_t fileId)
    {
        return SteamUGC.GetItemState(fileId);
    }

    public uint FetchSubscribedItems()
    {
        CheckInitialized();

        uint numSubscribedItems = SteamUGC.GetNumSubscribedItems();

        subscribedItems = new PublishedFileId_t[numSubscribedItems];
        uint count = SteamUGC.GetSubscribedItems(subscribedItems, numSubscribedItems);

        return count;
    }

    public bool HasFetchedSubscribedItems()
    {
        CheckInitialized();

        return subscribedItems != null;
    }

    public bool AllSubscribedItemsAreInstalled()
    {
        CheckInitialized();

        if (!HasFetchedSubscribedItems())
        {
            throw new Exception("No subscribed items list has been fetched");
        }

        foreach (PublishedFileId_t itemId in subscribedItems)
        {
            if (!IsSubscribedItemInstalled(itemId))
            {
                return false;
            }
        }

        return true;
    }

    public bool IsSubscribedItemInstalled(PublishedFileId_t itemId)
    {
        return (SteamUGC.GetItemState(itemId) & (uint) EItemState.k_EItemStateInstalled) != 0;
    }

    public string[] GetSubscribedItemPaths()
    {
        CheckInitialized();

        if (!HasFetchedSubscribedItems())
        {
            throw new Exception("No subscribed items list has been fetched");
        }

        string[] paths = new string[subscribedItems.Length];

        ulong size;
        uint timestamp;
        string folderPath;

        for (int i = 0; i < subscribedItems.Length; i++)
        {
            SteamUGC.GetItemInstallInfo(subscribedItems[i], out size, out folderPath, 0, out timestamp);
            paths[i] = folderPath;
        }

        return paths;
    }

    public string GetSubscribedItemPath(PublishedFileId_t id)
    {
        ulong size;
        uint timestamp;
        string folderPath;

        // No idea what this is, the facepunch implementation uses 4096 here so I guess we'll do it too!
        uint cchFolderSize = 4096;

        SteamUGC.GetItemInstallInfo(id, out size, out folderPath, cchFolderSize, out timestamp);

        return folderPath;
    }

    public string GetSteamNick()
    {
        return SteamFriends.GetPersonaName();
    }

    public ulong GetSteamId()
    {
        return SteamUser.GetSteamID().m_SteamID;
    }

    public void DropCurrentItem()
    {
        currentItem = null;
    }

    void SetError(string error)
    {
        Debug.LogError(error);
        errorMessage = error;
    }

    public void OpenCommunityFilePage(PublishedFileId_t itemId)
    {
        OpenUrl(COMMUNITY_FILE_PAGE_URL + itemId.m_PublishedFileId, true);
    }

    public void OpenUrl(string url, bool inSteamOverlay = true)
    {
        if (inSteamOverlay)
        {
            CheckInitialized();
            SteamFriends.ActivateGameOverlayToWebPage(url);
        }
        else
        {
            Application.OpenURL(url);
        }
    }

    public class WorkshopItem
    {

        public enum Visibility { NoChange, Private, FriendsOnly, Public }

        public PublishedFileId_t itemId;
        public string title;
        public string description;
        public string previewImagePath;
        public Visibility visibility;

        public WorkshopItem(PublishedFileId_t itemId)
        {
            this.itemId = itemId;
            visibility = Visibility.NoChange;
        }

        public override string ToString()
        {
            return "WokshopItem #" + itemId.m_PublishedFileId + ": " +
                title + ", " +
                description + ", ";
        }
    }

    public struct UGCQueryResult
    {
        public SteamUGCDetails_t details;
        public string previewImageURL;
    }
}