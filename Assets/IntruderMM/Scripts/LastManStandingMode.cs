﻿using UnityEngine;

public class LastManStandingMode : Photon.MonoBehaviour
{
    public bool on = false;
    public GameObject[] objectsToActivate;
    public GameObject[] objectsToDeactivate;
    public int defaultZoneCount = 6;
    public int defaultRoundLength = 75;

}
