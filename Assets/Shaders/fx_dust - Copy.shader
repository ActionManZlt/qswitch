﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/FX_Dust" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
    SubShader {
	Tags {
	"Queue" = "Transparent"
	"ForceNoShadowCasting" = "True"
	}
	GrabPass { "_GrabTexture" }

        Pass {
			ZWrite Off
			Cull Back
			Blend One One
			
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct vertOut {
                float4 pos:SV_POSITION;
                float2 uv:TEXCOORD0;
                float3 normal:TEXCOORD1;
            };
			
			sampler2D _MainTex;

            vertOut vert(appdata_base v) {
                vertOut o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.normal = mul(UNITY_MATRIX_MV, float4(v.normal,0));
                o.uv = v.texcoord.xy;
                return o;
            }

            fixed4 frag(vertOut i) : COLOR0 {
			
				float2 uv1 = i.uv;
				float2 uv2 = i.uv;
				uv1 += _Time.xx * float2( 0.7, 2)*0.1;
				uv2 += _Time.xx * float2(-0.7, 1)*0.1;
				
				float3 col1 =  tex2D(_MainTex, uv1*2).rgb;
				float3 col2 =  tex2D(_MainTex, uv2*2).rgb;
				
				float3 col = saturate(col1*col2*2-0.2);
				
				col *= float3(0.4,0.4,0.4);
				
				float a = frac(i.uv.y) ;
				
				a*=a;
				
				a *= i.normal.z;
				
				return float4(col*a,1);
            }

            ENDCG
        }
    }
}
